// Copyright Epic Games, Inc. All Rights Reserved.

#include "MatrixUnrealDemo.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MatrixUnrealDemo, "MatrixUnrealDemo" );
